#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <time.h> /* time */
#include <thread>


using namespace std;

int SocketFD;
vector<vector<string> > positions;

void exitProccess(int a) {
  write(SocketFD, "#", 1);
  exit(0);
}

void send(string message){
    
    int n;
    n = write(SocketFD, message.c_str(), message.size());
}

void Listen(int SocketFD)
{
  char read_buffer[256];
  int n;

  do
  {
    n = read(SocketFD, read_buffer, 256);
    cout << "Server says: " << read_buffer << endl;
    
    if(read_buffer[1]=='p'){
      //Positions matrix recieved
      positions.clear();
      int sz = (read_buffer[2]-'0')*100 + (read_buffer[3]-'0')*10 + (read_buffer[4]-'0');//Size of the matrix
      string aux;
      for(int i=0; i<sz; ++i){
        aux+=read_buffer[5+i];
      }
      for(int i=0; i<sz/5; ++i){
        vector<string> player(3);
        player[0]=aux[i*5];
        player[1]+=aux[i*5+1];
        player[1]+=aux[i*5+2];
        player[2]+=aux[i*5+3];
        player[2]+=aux[i*5+4];
        positions.push_back(player);
      }
      /*Printing the postion matrix*/
      for(int i=0; i<positions.size(); ++i){
          for(int j=0; j<positions[i].size(); ++j){
              cout<<positions[i][j]<<" ";
          }
          cout<<endl;
      }

    }
  }
  while (read_buffer[0] != '#');

}

int main(void)
{

  // Prevent CTRL + C
  signal(SIGINT, exitProccess);

  // Clean random
  srand(time(NULL));
  struct sockaddr_in stSockAddr;
  int Res;
  SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

  int n;

  if (-1 == SocketFD)
  {
    perror("cannot create socket");
    exit(EXIT_FAILURE);
  }

  memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(3000);

  Res = inet_pton(AF_INET, "192.168.1.36", &stSockAddr.sin_addr);

  if (0 > Res)
  {
    perror("error: first parameter is not a valid address family");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }
  else if (0 == Res)
  {
    perror("char string (second parameter does not contain valid ipaddress");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  if (-1 == connect(SocketFD, (const struct sockaddr *)&stSockAddr, sizeof(struct sockaddr_in)))
  {
    perror("connect failed");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  string foo;

  thread(Listen, SocketFD).detach();

  while(foo!="#"){
      cin>>foo;
      send(foo);
  }

  write(SocketFD, "#", 1);

  return 0;
}