#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <vector>
#include <time.h> /* time */
#include <thread>


using namespace std;

int SocketFD;

void exitProccess(int a) {
  write(SocketFD, "#", 1);
  exit(0);
}

void send(string message){
    
    int n;
    n = write(SocketFD, message.c_str(), message.size());
}

int main(void)
{

  // Prevent CTRL + C
  signal(SIGINT, exitProccess);

  // Clean random
  srand(time(NULL));
  struct sockaddr_in stSockAddr;
  int Res;
  SocketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

  int n;

  if (-1 == SocketFD)
  {
    perror("cannot create socket");
    exit(EXIT_FAILURE);
  }

  memset(&stSockAddr, 0, sizeof(struct sockaddr_in));

  stSockAddr.sin_family = AF_INET;
  stSockAddr.sin_port = htons(4000);

  Res = inet_pton(AF_INET, "192.168.1.36", &stSockAddr.sin_addr);

  if (0 > Res)
  {
    perror("error: first parameter is not a valid address family");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }
  else if (0 == Res)
  {
    perror("char string (second parameter does not contain valid ipaddress");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  if (-1 == connect(SocketFD, (const struct sockaddr *)&stSockAddr, sizeof(struct sockaddr_in)))
  {
    perror("connect failed");
    close(SocketFD);
    exit(EXIT_FAILURE);
  }

  string foo;

  while(foo!="#"){
      cin>>foo;
      send(foo);
  }

  write(SocketFD, "#", 1);

  return 0;
}