/* Server code in C */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <iostream>
#include <thread>
#include <vector>
#include <map>

#define REGISTER 'r' //id r posx posy Example: Ar1515
#define MOVEMENT 'm'

using namespace std;

vector<int> players;
bool kill = false;
vector<vector<string> > positions;
/*
Positions example:
A 15 14
B 13 12
C 0 0
*/

void Broadcast(const char* message, int size) {

  for (int i = 0; i < players.size(); ++i) {
    write(players[i], message, size);
  }

}

void GetAction(char* message, int size) {
  Broadcast(message, size);
}

void SendPositions(){

  string aux;
  int sz;
  sz=positions.size()*5;
  aux+="sp"; //Message from server (s) with the positions matrix (p)

  aux+=to_string(sz/100);//Size of the matrix
  aux+=to_string(sz/10);
  aux+=to_string(sz%10);

  for(int i=0; i<positions.size(); ++i){//Matrix
      for(int j=0; j<positions[i].size(); ++j){
          aux+=positions[i][j];
      }
  }
  Broadcast(aux.c_str(), aux.size());
}

void ListenPlayer(int ConnectFD) {

  char buffer[256];
  int n;
  
  do
  {
    buffer[0] = 0;
    n = read(ConnectFD, buffer, 255);
    buffer[n] = 0;
    cout<<"Received from a player: "<<buffer<<endl;

    if(buffer[1]==REGISTER){
        /*Register new ship*/
        vector<string> newShip(3);
        newShip[0] = buffer[0];
        newShip[1] += buffer[2];
        newShip[1] += buffer[3];
        newShip[2] += buffer[4];
        newShip[2] += buffer[5];
        positions.push_back(newShip);

        /*Printing the postion matrix*/
        for(int i=0; i<positions.size(); ++i){
            for(int j=0; j<positions[i].size(); ++j){
                cout<<positions[i][j]<<" ";
            }
            cout<<endl;
        }
        /*Send the position matrix to all the clients*/
        SendPositions();
        
    }

    if(buffer[1]==MOVEMENT){
      
      int idPlayer=-1;
      for(int i=0; i<positions.size(); ++i){

        if(positions[i][0][0]==buffer[0]){
          idPlayer=i;
          break;
        }
      }

      string newX, newY;
      newX+=buffer[2];
      newX+=buffer[3];
      newY+=buffer[4];
      newY+=buffer[5];
      positions[idPlayer][1]=newX;
      positions[idPlayer][2]=newY;
      /*Printing the postion matrix*/
      for(int i=0; i<positions.size(); ++i){
          for(int j=0; j<positions[i].size(); ++j){
              cout<<positions[i][j]<<" ";
          }
          cout<<endl;
      }
      /*Send the position matrix to all the clients*/
      SendPositions();
    }
    
    //GetAction(buffer, n);

  } while (*buffer != '#');

  for(int i = 0; i < players.size(); ++i) {
    if (players[i] == ConnectFD) {
      close(ConnectFD);
      players.erase(players.begin() + i);
    }
  }
  
}

void ListenClient(int ConnectFD) {

  char buffer[256];
  int n;
  
  do
  {
    buffer[0] = 0;
    n = read(ConnectFD, buffer, 255);
    buffer[n] = 0;
    cout<<"Received from a client: "<<buffer<<endl;
    write(ConnectFD, buffer, n);

  } while (*buffer != '#');

  close(ConnectFD);
}

void GetConnection(int &SocketFD, socklen_t &size, int port){

  struct sockaddr_in stSockAddr;
  struct sockaddr_in cli_addr;

  if ((SocketFD = socket(AF_INET, SOCK_STREAM, 0)) == -1)
  {
    perror("Socket");
    exit(1);
  }

  if (setsockopt(SocketFD, SOL_SOCKET, SO_REUSEADDR, "1", sizeof(int)) == -1)
  {
    perror("Setsockopt");
    exit(1);
  }

  stSockAddr.sin_family = AF_INET;

  stSockAddr.sin_port = htons(port);

  stSockAddr.sin_addr.s_addr = INADDR_ANY;

  if (bind(SocketFD, (struct sockaddr *)&stSockAddr, sizeof(struct sockaddr)) == -1)
  {
    perror("Unable to bind");
    exit(1);
  }

  if (listen(SocketFD, 5) == -1)
  {
    perror("Listen");
    exit(1);
  }

  size = sizeof(stSockAddr);

  while(!kill)
  {
    int ConnectFD = accept(SocketFD, NULL, NULL);
    if(port == 3000){
        thread(ListenPlayer, ConnectFD).detach();
        players.push_back(ConnectFD);
    }
    else if (port == 4000)
        thread(ListenClient, ConnectFD).detach();
    
  }
}

void ListenForClose(){
    string foo;
    while(!kill){
        cin>>foo;
        cout<<foo<<endl;
        if(foo=="exit")
            kill=true;
    }
}

int main(int argc, char** argv) {
  socklen_t size;
  int SocketFD;
  int SocketFD1;
  int n;

  thread acceptingPlayers(GetConnection, ref(SocketFD), ref(size), 3000);
  thread acceptingClients(GetConnection, ref(SocketFD1), ref(size), 4000);
  thread listeningForClose(ListenForClose);

  listeningForClose.join();
  acceptingPlayers.detach();
  acceptingClients.detach();
  
  close(SocketFD);
  close(SocketFD1);
  cout<<"Bye  bye :D"<<endl;
  return 0;
}